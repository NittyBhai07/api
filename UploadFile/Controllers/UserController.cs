﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UploadFile.Controllers
{
    public class UserController : ApiController
    {
        string strCOnnectionString = ConfigurationSettings.AppSettings["VisitorsManagement"].ToString();
        string strResponseCode, strResponseDescription, strErrorMessage;
        
        [HttpPost, ActionName(name: "SaveData")]
        public string SaveData(List<ABC> DA)
        {
            //return "123";
            //DataTable dt = new DataTable();

            foreach (var details in DA)
            {
                ABC objABC = new ABC();
                objABC.Name = details.Name;
                objABC.Age = details.Age;
                objABC.City = details.City;

                try
                {
                    using (SqlConnection con = new SqlConnection(strCOnnectionString))
                    {
                        SqlCommand cmd = new SqlCommand("InsertDetails", con);
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter objParam = new SqlParameter();

                        objParam = new SqlParameter("@Name", SqlDbType.VarChar);
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = objABC.Name;
                        cmd.Parameters.Add(objParam);

                        objParam = new SqlParameter("@Age", SqlDbType.VarChar);
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = objABC.Age;
                        cmd.Parameters.Add(objParam);

                        objParam = new SqlParameter("@City", SqlDbType.VarChar);
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = objABC.City;
                        cmd.Parameters.Add(objParam);


                        objParam = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, -1);
                        objParam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objParam);

                        objParam = new SqlParameter("@ResponseCode", SqlDbType.VarChar, -1);
                        objParam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objParam);

                        objParam = new SqlParameter("@ResponseDescription", SqlDbType.VarChar, -1);
                        objParam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objParam);

                        cmd.ExecuteNonQuery();

                        strResponseCode = cmd.Parameters["@ResponseCode"].Value.ToString();
                        strResponseDescription = cmd.Parameters["@ResponseDescription"].Value.ToString();
                        strErrorMessage = cmd.Parameters["@ErrorMessage"].Value.ToString();
                        
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                
            }
            return strResponseDescription;
        }

        public class ABC
        {
            public string Name { get; set; }
            public string Age { get; set; }
            public string City { get; set; }
        }
    }
}
