﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UploadFile.Models
{
    public class UserDetail
    {
        public string Name { get; set; }
        public string Age { get; set; }
        public string City { get; set; }
    }
}